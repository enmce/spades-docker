FROM ubuntu:14.04

MAINTAINER "Viktor Svekolkin" <enmce@yandex.ru>

RUN apt-get update && apt-get install -y \
	wget \
	tar \
	python

ENV VERSION 3.5.0

RUN wget -P /opt http://spades.bioinf.spbau.ru/release${VERSION}/SPAdes-${VERSION}-Linux.tar.gz

WORKDIR /opt

RUN tar -xzf SPAdes-${VERSION}-Linux.tar.gz

WORKDIR /opt/SPAdes-${VERSION}-Linux/bin

ENTRYPOINT ["python", "spades.py"]




	
